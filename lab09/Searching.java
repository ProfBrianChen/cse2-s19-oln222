/// Liv Newman, CSE 2, lab09 
/// this program will use different methods to search for integers in arrays
///

import java.util.Scanner;
import java.util.Arrays;
public class Searching {
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Do you want to do a linear or binary search?");
    String binLin = myScanner.next();
    if(binLin.equals("linear")){
      System.out.println("What array size?");
      int size = myScanner.nextInt();
      System.out.println("What term are you looking for?");
      int search = myScanner.nextInt();
      int[] randArray = genRand(size);
      System.out.println(Arrays.toString(randArray));
      System.out.println(linSearch(randArray, search));      
    }
    else if(binLin.equals("binary")){
      System.out.println("What array size?");
      int size = myScanner.nextInt();
      System.out.println("What term are you looking for?");
      int search = myScanner.nextInt();
      int[] ascendArray = genAscend(size);
      System.out.println(Arrays.toString(ascendArray));
      System.out.println(binSearch(ascendArray, search));
    }
    
    else{
    }
  }     //ending main method
  
  
  public static int[] genRand(int size){
    int[] list = new int[size];   //declaring array of size size
    for(int i=0; i<size; i++){    //for each value
      int number = (int)(Math.random()*size);   //generating random ints
      list[i] = number;   //inputting each random int into the array for that #
    }
    return list;
  }
  
  public static int[] genAscend(int size){
    int[] list = new int[size];   //declaring array of size size   
    list[0] = (int)(Math.random()*100);    //generating random int for 0th value
    boolean valid  = false;
    for(int i=1; i<size; i++){   //for each value after the 0th,
      //IF THESE ACTUALLY NEED TO BE GENERATED up to the length, then change 100 to size for these 2 values
      do {
        valid = false;
        int number = (int)(Math.random()*100);   //generating random ints
        if(number >= list[i-1]){   //making it ascending
          list[i] = number;   //inputting each random int into the array for that #
          valid = true;
        }
        else{
          //valid = false
        }
      }     //ending do section
      while(!valid);
        
    }   //ending for loop
    return list;
  }   //ending genAscend method
  
  
  public static int linSearch(int[] list, int value){
    for(int i=0; i<(list.length-1); i++){   //searching each term
      if(list[i] == value){   //if that value at the index is = value
        return i;   //returning index
      }
    }
    return -1;
    
  }   //ending this method
  
  public static int binSearch(int[] list, int value){
    int low = 0;
    int high = list.length - 1;
    int mid;
    while (low <= high){
      mid = (high + low)/2;
      if (value > list[mid]){
        low = mid + 1;
      } else if (value < list[mid]){
        high = mid - 1;
      } else if (value == list[mid]){   //when the value is equal to the value wanted,
        return mid;   //return the index of that value
      }
    }
    return -1;
    
  }   //ending this method
  
}   //closing class













