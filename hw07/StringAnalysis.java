/// Liv Newman, CSE 2, hw07
/// this program will check characters in a string
///

import java.util.Scanner;   //importing Scanner
public class StringAnalysis{    //opening class
  public static void main(String args[]){   //opening main method
    System.out.println("Enter a string.");    //asking for a string
    Scanner myScanner = new Scanner(System.in);   
    String input = myScanner.nextLine();    //scanner to take in entire line of input
    System.out.println("Check all numbers or just some? Enter all or some.");
    String amount = myScanner.next();   //user responds all or some
    boolean isLetters = false;    //initializing isLetters 
    if (amount.equals("all")){    //if user wants all checked
      isLetters = check(input);   //all characters will all be checked
    }
    else if (amount.equals("some")){    //if user wants some checked
      System.out.println("How many characters?");    //asking for how many chars to be checked
      int length = myScanner.nextInt();
      isLetters = check(input, length);   //the characters will be checked up to that length
    }
    if(isLetters){    //print message if all characters are letters
      System.out.println("These are all letters.");   
    }
    else{   //print message if characters are not all letters
      System.out.println("These are not all letters.");
    }
  }     //ending main method
  
  public static boolean check(String str){    //opening method to check if all chars will be checked
    for (int i=0; i<str.length(); i++){   //loop that checks all characters
      if (!(Character.isLetter(str.charAt(i)))){    //if this character is not a letter
        return false;   //it returns false
      }
    }
    return true;    //otherwise it returns true
  }   //closing method
  
  public static boolean check(String str, int length){    //opening method to check chars up to length amount
    for (int i=0; i<length; i++){   //loop that checks characters up to length amount
      if (!(Character.isLetter(str.charAt(i)))){    //if this character is not a letter
        return false;   //it returns false
      }
    }
    return true;    //otherwise it returns true
  }   //closing class
  
}   //ending class