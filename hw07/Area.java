/// Liv Newman, CSE 2, hw07
/// this program will calculate the area of a
//circle, rectangle, or triangle
///

import java.util.Scanner;   //importing scanner
public class Area{    //opening class
  public static void main(String[] args){   //opening main method
    System.out.println("Please enter a shape.");    //asking for shape
    Scanner myScanner = new Scanner(System.in); 
    String shape = myScanner.next();
    shape = shape.toLowerCase();    //making all letters lowercase
    while(!check(shape)){   //loop to wait for an input until it is a correct shape
      System.out.println("Incorrect input. Please choose rectangle, circle, or triangle.");
      shape = myScanner.next();
      shape = shape.toLowerCase();
    }   //closing loop
    
    double area = 0;   //initializing area
    if(shape.equals("rectangle")){    //for a rectangle
      System.out.println("Enter a width.");   //ask for width
      double width = myScanner.nextDouble(); 
      System.out.println("Enter a length.");    //ask for length
      double length = myScanner.nextDouble();
      area = rectArea(width, length);   //calling method for area of a rectangle
    }
    else if (shape.equals("triangle")){   //for a triangle
      System.out.println("Enter a base.");    //ask for a base
      double base = myScanner.nextDouble(); 
      System.out.println("Enter a height.");    //ask for a height
      double height = myScanner.nextDouble();
      area = triArea(base, height);   //calling method for area of a triangle
    }
    else if (shape.equals("circle")){   //for a circle
      System.out.println("Enter a radius.");    //ask for radius
      double radius = myScanner.nextDouble(); 
      area = circArea(radius);    //calling method for area of a circle
    }
    System.out.println("The area of this shape is " + area);    //printing area of shape
  }   //ending main method

  public static boolean check(String shape){    //checking that the shape is one of the correct inputs
    if(shape.equals("rectangle")){    //correct shape if rectangle
      return true;
    }
    else if (shape.equals("triangle")){   //correct shape if triangle
      return true;
    }
    else if (shape.equals("circle")){   //correct shape if circle
      return true;
    }
    else{   //if not one of these three, it is not a correct shape
      return false;
    }
  }     //ending check method

  public static double rectArea(double width, double length){   //area method of a rectangle
    return width*length;    //returning area
  }   //ending method for rectangle area

  public static double triArea(double base, double height){   //area method of a triangle
    return 0.5*base*height;   //returning area
  }   //ending method for triangle area

  public static double circArea(double radius){   //area method of a circle
    return Math.PI*radius*radius;   //returning area
  }   //ending method for circle area

}   //closing class
  