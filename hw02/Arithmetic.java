//////
///CSE 02 Arithmetic
///
public class Arithmetic{
public static void main (String args []){
  ///setting main method and class
  //number of pairs of pants
  int numPants = 3;
  //Cost per pair of pants
  double pantsPrice = 34.98;
  
  //Number of sweatshirts
  int numShirts = 2;
  //Cost per shirt
  double shirtPrice = 24.99;
  
  //Number of belts
  int numBelts = 1;
  //cost per belts
  double beltCost = 33.99;
  
  //the tax rate
  double paSalesTax = 0.06;
 
  
  double totalCostOfPants; 
  totalCostOfPants = (numPants*pantsPrice);
  //total cost of pants before sales tax
    
  double totalCostOfSweatshirts;
  totalCostOfSweatshirts = (numShirts*shirtPrice);
  //total cost of sweatshirts before sales tax
    
  double totalCostOfBelts;
  totalCostOfBelts = (numBelts*beltCost);
  //total cost of belts before sales tax
  
  double salesTaxPants;
  salesTaxPants = (int)(paSalesTax*totalCostOfPants*100)/100.0;
  //cost of sales tax of pants, explicitly converted to integer
  
  double salesTaxSweatshirts;
  salesTaxSweatshirts = (int)(paSalesTax*totalCostOfSweatshirts*100)/100.0;
  //cost of sales tax of sweatshirts, explicitly converted to integer
  
  double salesTaxBelts;
  salesTaxBelts = (int)(paSalesTax*totalCostOfBelts*100)/100.0;
  //cost of sales tax of belts, explicitly converted to integer
  
  double totalClothes;
  totalClothes = (totalCostOfBelts+totalCostOfSweatshirts+totalCostOfPants);
  //total cost of original prices before sales tax
  
  double totalTax;
  totalTax = (float)((salesTaxBelts+salesTaxSweatshirts+salesTaxPants)*100)/100.0;
  //total cost of sales tax, explicitly converted to integer
  
  double totalPurchse;
  totalPurchse = (totalClothes+totalTax);
  //total cost of original price and sales tax
  
  
  System.out.println("The original cost of pants is $"
         +totalCostOfPants+".");
  System.out.println("The original cost of sweatshirts is $"
         +totalCostOfSweatshirts+".");
  System.out.println("The original cost of belts is $"
         +totalCostOfBelts+".");
  System.out.println("The sales tax on pants is $"
         +salesTaxPants+".");
  System.out.println("The sales tax on sweatshirts is $"
         +salesTaxSweatshirts+".");
  System.out.println("The sales tax on belts is $"
         +salesTaxBelts+".");
  System.out.println("The total cost of purchases before sales tax is $"
         +totalClothes+".");
  System.out.println("The total cost of sales tax is $"
         +totalTax+".");
  System.out.println("The total cost of this transaction including sales tax is $"
         +totalPurchse+".");
   
  
  
  
}
                   

}