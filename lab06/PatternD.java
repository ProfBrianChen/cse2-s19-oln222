/// Liv Newman, CSE 2, lab06
/// this program will print Pattern D
///

import java.util.Scanner;   //importing Scanner
public class PatternD{   //declaring class
  public static void main(String[] args){   //declaring mian method
    System.out.println("Enter an integer 1 to 10.");   //asking for length, to be # of rows
    Scanner myScanner = new Scanner(System.in);   //declaring a new Scanner
    
    while(!myScanner.hasNextInt()){    //checking that the input is an integer
      System.out.println("Incorrect input. Enter an integer.");   //printing error message
      myScanner.next();   //waiting for the value to be input again after the message
    }   //ending while loop
    
    int length = myScanner.nextInt();   //declaring length as value input and storing it in length
    
    while(length > 10 || length < 1){    //checking input has correct bounds
      System.out.println("The input must be 1 to 10.");
      length = myScanner.nextInt();
    }   //ending while loop
    //now the input is an integer between 1 and 10
    
    String temp = "";   //setting temp as an empty String to later be rewritten
    for(int row = 0; row<length; row++){    //for loop determining # of rows
      for(int col=length-row; col>=1; col--){   //inner for loop determining what #s to print
        temp = temp + col + " ";    //rewriting temp as the number and a space
      }   //ending inner for loop
      System.out.println(temp);   //printing one row of text
      temp="";  //resetting temp as an emtpy String
    }   //ending outer for loop
    
  }   //closing main method
}     //closing class