/// Liv Newman, CSE 2, lab06
/// this program will print Pattern B
///
///

import java.util.Scanner;   //importing Scanner
public class PatternB{   //declaring class
  public static void main(String[] args){   //declaring mian method
    System.out.println("Enter an integer 1 to 10.");   //asking for length, to be # of rows
    Scanner myScanner = new Scanner(System.in);   //declaring a new Scanner
    
    while(!myScanner.hasNextInt()){    //checking that the input is an integer
      System.out.println("Incorrect input. Enter an integer.");   //printing error message
      myScanner.next();   //waiting for the value to be input again after the message
    }   //ending while loop
    
    int length = myScanner.nextInt();   //declaring length as value input and storing it in length
    
    while(length > 10 || length < 1){    //checking input has correct bounds
      System.out.println("The input must be 1 to 10.");
      length = myScanner.nextInt();
    }   //ending while loop
    //now the input is an integer between 1 and 10
    
    //String temp = "";   //naming an empty string to be rewritten over later
    for(int row=1; row<=length; row++){   //starting row at 1, while true that #rows<length, then making numRows=length
      for(int col=1; col<=length-row+1; col++){
        //temp = temp + col + " ";    //overriding temp w a modified version of itself
        System.out.print(col+" ");
      }
      System.out.println();
     //System.out.println(temp);   //printing out each row
    }   //ending for loop
    
  }   //ending main method
}   //ending class