/// Liv Newman, CSE 2, lab06
/// this program will print Pattern C
///

import java.util.Scanner;   //importing Scanner
public class PatternC{   //declaring class
  public static void main(String[] args){   //declaring mian method
    System.out.println("Enter an integer 1 to 10.");   //asking for length, to be # of rows
    Scanner myScanner = new Scanner(System.in);   //declaring a new Scanner
    while(!myScanner.hasNextInt()){    //checking that the input is an integer
      System.out.println("Incorrect input. Enter an integer.");   //printing error message
      myScanner.next();   //waiting for the value to be input again after the message
    }   //ending while loop
    
    int length = myScanner.nextInt();   //declaring length as value input and storing it in length
    
    while(length > 10 || length < 1){    //checking input has correct bounds
      System.out.println("The input must be 1 to 10.");
      length = myScanner.nextInt();
    }   //ending while loop
    //now the input is an integer between 1 and 10
    
    String temp = "";   //declaring temp as empty string 
    for(int row=1; row<=length; row++){   //for loop to determine # of rows
      for(int col=length; col>=row; col--){   //for loop to determing # of spaces
        temp = temp + " ";    //rewriting temp as a space
      }   //ending first inner loop
      for(int col=row; col>=1; col--){    //for loop determinig #s to be printed
        temp = temp + col;    //rewriting temp as spaces and numbers
      }   //ending second inner loop
      System.out.println(temp);   //printing one row of text
      temp = "";    //resetting temp as empty String to start loop again
    }   //ending outer for loop
  } //ending main method
}  //ending class