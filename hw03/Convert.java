/// Liv Newman, CSE2, HW 3, problem 1
///this program will convert meters to inches, as doubles
///

import java.util.Scanner;    //using Scanner to prompt user with the following information
public class Convert{        //naming class
  public static void main(String[] args){   //naming main method
    //declaring instance of Scanner and calling Scanner construction
    Scanner myScanner = new Scanner( System.in );     
   
    
    //declaring meters and inches as doubles
    double meters, inches;
    //prompting user for distance in meters
    System.out.print("Enter the distance in meters: ");
    //allowing the user to use Scanner
    meters = myScanner.nextDouble();
    //using the value given by the user 
    //System.out.print(meters);
    
    
    //converting meters to inches
    inches = (int)(meters*39.3701*10000)/10000.0;
    //printing the how many inches as answer
    System.out.printf("%4.2f meters is %3.4f inches.\n",meters,inches);
    
    
    
  }
}