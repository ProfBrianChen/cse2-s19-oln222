/// Liv Newman, CSE2, HW 03, problem 2
///This will give the volume of a box as a result of 
///prompting user for dimensions of a box.
///

import java.util.Scanner;   //using Scanner to promt user
public class BoxVolume{
  public static void main(String[] args){   //naming main method
    //declaring instance of Scanner and calling Scanner construction
    Scanner myScanner = new Scanner(System.in);
    
    ////declaring width, length, height, and volume as ints
    int width, length, height, volume; 
    
    //printing width of box
    System.out.print("The width side of the box is: ");
    //allowing user to enter width of box
    width= myScanner.nextInt();
    
    //printing length of box
    System.out.print("The length of the box is: ");
    //allowing user to enter length of box
    length= myScanner.nextInt();
    
    //printing height of box
    System.out.print("The height of the box is: ");
    //allowing user to enter height of box
    height= myScanner.nextInt();
    
    
    //now we can calculate the volume with the dimensions
    volume= width*length*height;
    //printing output of volume of box
    System.out.println("The volume inside the box is: " + volume);
    
    
    
    
  }
  
}