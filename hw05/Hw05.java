///Liv Newman, CSE2, Hw 05
///this program will ask the user to enter information regarding a course
//including course number, department name, # times it meets in a week,
//time class starts, instructor name, and # of students


import java.util.Scanner;   //importing Scanner

public class Hw05{          //declaring class
  public static void main(String[] args){     //declaring main method
    Scanner keyboard = new Scanner(System.in);    //declaring a new Scanner
   
    System.out.println("Enter the course number.");     //asking for user to input the course number
    while(!keyboard.hasNextInt()){                      //if the user does not input an integer
        System.out.println("Wrong. Enter the course number.");    //it will print an error message and loop
        keyboard.next();                                      //until the correct form is printed
    }
    int courseNum = keyboard.nextInt();                 //retrives the input
    
    System.out.println("Enter the department name.");     //process repeats for department name as a string
    String deptName = keyboard.next();
    
    
    System.out.println("Enter the number of times it meets in a week.");    //process repeats for # meetings/week as integer
    while(!keyboard.hasNextInt()){
        System.out.println("Wrong. Enter the number of times it meets in a week.");
        keyboard.next();
    }
    int meetsPerWeek = keyboard.nextInt();
    
    
    System.out.println("Enter the time class starts.");   //process repeats for time class starts as string
    String startTime = keyboard.next();
    
    
    System.out.println("Enter the instructor's name.");   //process repeats for instructor name as string
    String instructorName = keyboard.next();
    
    
     System.out.println("Enter the number of students.");   //process repeats for # of students as int
    while(!keyboard.hasNextInt()){
        System.out.println("Wrong. Enter the number of students.");
        keyboard.next();
    }
    int numStudents = keyboard.nextInt();
    
    
    //printing all of the information collected
    System.out.println("This course number is " + courseNum ".");      
    System.out.println("The department is " + deptName".");
    System.out.println("It meets " + meetsPerWeek + " times per week.");
    System.out.println("It starts at " + startTime".");
    System.out.println("The instructor is " + instructorName".");
    System.out.println("There are " + numStudents + " students in the class.");
    
  }   //ending main method

}     //ending class