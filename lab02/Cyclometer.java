////
//// Liv Newman, lab 2/1/19, CSE2 Spring 2019, Prof Chen
//// Cyclometer.java
//// This program will record the data for the cyclometer, including number of minutes for each trip, number of counts for each trip, distance of each trip in miles, and distance for the 2 trips combined
////
public class Cyclometer {
  //main method required for every Java program
  public static void main (String [] args) {
     //our input data
    int secsTrip1=480;    //480 seconds for Trip 1, in integer
    int secsTrip2=3220;   //3220 seconds for Trip 2, in integer
    int countsTrip1=1561; //1561 of rotations for Trip 1, in integer
    int countsTrip2=9037; //9037 of rotations for Trip 2, in integer
    
    //our intermediate variables and output data
    double wheelDiameter=27.0;  //wheel diameter is 27.0 units, as a double
    double PI=3.14159;          //replacing value of pi, as double
    int feetPerMile=5280;       //naming # feet per mile, as integer bc it'll automatically cast to double anyway
    int inchesPerFoot=12;       //naming # inches per foot, as integer
    int secondsPerMinute=60;    //naming # seconds per minute, as integer
    double distanceTrip1;       //declaring dist of trip 1, as double
    double distanceTrip2;       //declaring dist of trip 2, as a double
    double totalDistance;       //declaring total distance, as a double
    
    System.out.println("Trip 1 took "+
            (secsTrip1/secondsPerMinute) +" minutes and had "+
            countsTrip1+" counts.");
            //this converts number of seconds to number of minutes and states the counts, for trip 2
    System.out.println("Trip 2 took "+ 
            (secsTrip2/secondsPerMinute)+" minutes and had "+
            countsTrip2+" counts.");
            //this converts number of seconds to number of minutes and states the counts, for trip 2
    
    distanceTrip1=countsTrip1*wheelDiameter*PI;
         //this is the distance in inches, bc each rotation is measure of the circumference
    distanceTrip1/=inchesPerFoot*feetPerMile;
         //gives distance in miles 
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
       
    //print out the output data
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
    
   }        //end of main method
}           //end of class    //brackets up to this point match











