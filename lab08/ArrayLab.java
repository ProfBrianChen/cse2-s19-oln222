/// Liv Newman, CSE 2, lab08
/// this program will create random arrays and calculate the min, max, and range
///

import java.util.Arrays;    
public class ArrayLab{    //opening class
  public static void main(String args[]){   //opening main method
    int size = (int)((Math.random()*50)+50);    //producing random size
    System.out.println("The size of the array is " + size + ".");
    int[] nums = new int[size];   //declaring array of ints
    for (int i=0; i<size; i++){   //for loop producing a random number of arrays
      int number = (int)(Math.random()*100);    //each integer
      nums[i] = number;   //putting each random number into the array
    }
    System.out.println("The original array is \n" + Arrays.toString(nums));   //printing the array
    System.out.println("The range is " + getRange(nums) + ".");
    System.out.println("The mean is " + getMean(nums) + ".");
    System.out.println("The standard deviation is " + getStDev(nums) + ".");
    shuffle(nums); //shuffle the array
    System.out.println("The shuffled array is \n" + Arrays.toString(nums));
  }   //ending main method
  
  public static int getRange(int[] list){   
    int max = 0;
    int min = 100;
    Arrays.sort(list);
    for (int i=0; i<list.length; i++){    //for every # in the array
      if (list[i] > max){    //max is updated with highest number if that # is greater than the previous max
        max = list[i];
      }
      else if (list[i] < min){   //if a # is greater than the previous min, it will be the new min
        min = list[i];
      }
    }
    System.out.println(max + " " + min);
    return max-min;
  }   //ending range method
  
  public static double getMean(int[] list){
    double mean = 0;
    int sum = 0;
    for(int i=0; i<list.length; i++){
      sum = sum + list[i];
    }
    mean = sum / list.length;
    return mean;
  }   //ending mean method
  
  public static double getStDev(int[] list){
    double mean = getMean(list);
    double sum = 0;
    for(int i=0; i<list.length; i++){
      //sum = sum + list[i];
      double numerator = Math.pow((list[i]-mean),2);
      sum += numerator;
    } 
    double underRad = sum / (list.length-1);
    double stDev = Math.sqrt(underRad);
    return stDev;
  }   //ending st dev method
  
  public static void shuffle(int[] list){
    for (int i=0; i<list.length; i++){
      int target = (int)(list.length * Math.random());
      int temp = list[target];
      list[target] = list[i];
      list[i] = temp;
    }
  }
  
}   //ending class










