//// Liv Newman, CSE2, Friday lab, Lab 3
////
//// This will get the original cost of the check,
//// the percentage tip,
//// the number of ways the check will be split,
//// and how much each person needs to pay. 
//// 
import java.util.Scanner;           //importing Scanner to use data
public class Check{               //required main method
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
           //declaring instance of Scanner and calling Scanner constructor
           //creating insatnce that will use STDIN for input
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
           //ready for original cost of check
    double checkCost = myScanner.nextDouble();
           //calls nextInt() method 
   
      //prompting for tip percentage 
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
           //prompting for percentage tip
    double tipPercent = myScanner.nextDouble();
           //user inputs percentage tip 
    tipPercent /=100;
           //converting percentage into decimal
    
      //prompting for number of people who went to dinner
    System.out.print("Enter the number of people who went out to dinner: ");
           //printing line of text to be prompted
    int numPeople = myScanner.nextInt();
           //user inputs number of people who went to dinner and Scanner takes it in
    
      //printing output for amount each group member needs to pay
    double totalCost;           //declaring total cost
    double costPerPerson;       //declaring cost per person
    int dollars, dimes, pennies;
        //whole dollar, dimes, pennies amount of cost for storing
        //decimal points of cost
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
        //getting the whole amount without decimal
    dollars = (int)costPerPerson;
        //getting dimes amount
        //(int) (6.73*10) % 10 -> 67 % 10 -> 7;
        //where the % (mod) operator returns remainder after division
        // 583%100 -> 83, 27%5 ->2
    dimes=(int)(costPerPerson*10)%10;     //putting # of dimes in the tenths place as decimal
    pennies=(int)(costPerPerson*100)%10;  //putting # of pennies in the hundredths place as decimal
    System.out.println("Each person in the group owes $" + dollars + "."
                      + dimes + pennies);
        //printing amount owed by each person
      
    
  }     //end of main method
}       //end of class