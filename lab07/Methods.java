/// Liv Newman, CSE 2, lab07
/// this program will make sentences of random components
///

import java.util.Random;    //importing Random
public class Methods{   //opening class
  public static void main(String args[]){   //opening main method
    String subject = subj();
    System.out.println("The " +adj()+" "+adj()+" "+subject+" "+ verb()+" the "+adj()+" "+object()+".");
    System.out.println(subject + " used " + object() + " to " + verb() + " " + object() + " at the " + adj() + " " + object() + ".");
    System.out.println("That " + subject + " " + verb() + " her " + object() + "!");
  }//ending main method
  
  public static String adj(){   //method for adjective
    Random rand = new Random();
    int num = rand.nextInt(10);
    switch(num){
      case 0:
        return "silly";
     case 1:
        return "sleepy";
     case 2:
        return "stupid";
     case 3:
        return "slimy";
     case 4:
        return "sweaty";
     case 5:
        return "sly";
     case 6:
        return "silver";
     case 7:
        return "slippery";
     case 8:
        return "swaggy";
     case 9:
        return "sneezy";
     }
    return "";
  }   //ending adjective method
  
  public static String subj(){    //method for subject
    Random rand = new Random();
    int num = rand.nextInt(10);
    switch(num){
      case 0:
        return "creature";
     case 1:
        return "carrot";
     case 2:
        return "car";
     case 3:
        return "child";
     case 4:
        return "character";
     case 5:
        return "cat";
     case 6:
        return "cow";
     case 7:
        return "candle";
     case 8:
        return "cradle";
     case 9:
        return "crow";
     }
     return "";

  }   //ending subject method
  
  public static String verb(){    //method for verb
    Random rand = new Random();
    int num = rand.nextInt(10);
    switch(num){
      case 0:
        return "threw";
     case 1:
        return "turned";
     case 2:
        return "traced";
     case 3:
        return "trapped";
     case 4:
        return "talked";
     case 5:
        return "taught";
     case 6:
        return "toppled";
     case 7:
        return "teased";
     case 8:
        return "took";
     case 9:
        return "tripped";
     }
    return "";
  }   //ending verb method
  
  public static String object(){    //method for object
    Random rand = new Random();
    int num = rand.nextInt(10);
    switch(num){
      case 0:
        return "book";
     case 1:
        return "bat";
     case 2:
        return "beets";
     case 3:
        return "bears";
     case 4:
        return "bee";
     case 5:
        return "ball";
     case 6:
        return "ballroom";
     case 7:
        return "bond";
     case 8:
        return "blueberry";
     case 9:
        return "banana";
     }
    return "";
  }   //ending object method
}   //ending class