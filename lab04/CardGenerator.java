///
///Liv Newman, CSE2, Lab 04, 2/15/19
///This code will randomly select a card
///from a deck of cards
public class CardGenerator{       //inputting class
  public static void main (String[] args){ //inputting main method
    int card = (int)((Math.random() * 52)+1); //generating random integer 1-52
      String cardValue = "";    //declaring cardValue as integer
      String Suit = "";  //declaring card suit as String
      
      if(card >= 1 && card <= 13){    //if card is btwn 1-13,
          Suit = "Diamonds";           //then it is a diamond
      }
      else if(card >= 14 && card <= 26){    //if card is btwn 14-26,
          Suit = "Clubs";             //then it is a clubs      
      }
      else if(card >= 27 && card <= 39){    //if card is btwn 27-39,
          Suit = "Hearts";            //then it is a heart        
      }  
    else if (card >= 40 && card <= 52){    //if card is btwn 40-52,
          Suit = "Spades";           //then it is a spade
      } 
    
    
     switch (card%13){                //gives the remainder of a card value when divided by 13
       case 0: cardValue = "King";    //the value is a King
         break;                       
       case 1: cardValue = "Ace";      //if the remainder is 1, the card is an Ace
         break;
       case 2: cardValue = "2";      //if the remainder is 2, the card is a 2
         break;
       case 3: cardValue = "3";      //if the remainder is 3, the card is a 3
         break;
       case 4: cardValue = "4";      //if the remainder is 4, the card is a 4
         break;
       case 5: cardValue = "5";      //if the remainder is 5, the card is a 5
         break;
       case 6: cardValue = "6";      //if the remainder is 6, the card is a 6
         break;
       case 7: cardValue = "7";      //if the remainder is 7, the card is a 7
         break;
       case 8: cardValue = "8";      //if the remainder is 8, the card is an 8
         break;
       case 9: cardValue = "9";      //if the remainder is 9, the card is a 9
         break;
       case 10: cardValue = "10";    //if the remainder is 10, the card is a 10
         break;
       case 11: cardValue = "Jack";    //if the remainder is 11, the card is a Jack
         break;
       case 12: cardValue = "Queen";      //if the remainder is 12, the card is a Queen
         break;
     }
    
    
    
    System.out.println("You picked the " + cardValue + " of " + Suit ); //printing out the final statemetn
   
    
  }
  
}