/// Liv Newman, CSE 2, lab6
/// this program will print a "twist" on the screen
///


import java.util.Scanner;       //importing Scanner

public class TwistGenerator{    //naming class
  public static void main(String[] args){   //naming main method
    Scanner keyboard = new Scanner(System.in);    //making a new scanner
    
    //declare and initialize length with an invalid value
    //so the program doesn't run accidentally
    int length = -1;  
    System.out.println("Enter a length.");    //asking user to enter length
    boolean goodLength = false;   //sets up a boolean that determines if it's valid
    while(goodLength ==false){    //code will keep repeating until valid input is entered
      if (keyboard.hasNextInt()){   //length must be an integer
        length=keyboard.nextInt();    //sets the input equal to length
          if(length >=0){         //length must be positive
            goodLength =!goodLength;    //setting goodLength = true so the code won't run again
          } else {System.out.println("Wrong. Enter a length as an int.");    
        }
      }else{System.out.println("Wrong. Enter a length as an int.");    
        keyboard.next();}
      }     //ending this while loop
      
    String line1="", line2="", line3 = "";    //creating empty strings to hold each line's output
    for(int i=0; i<length; i++){    //generate the # of lines equal to the input
      switch(i%3){    //divide by 3 to get each different part of pattern
        case 0:   //when remainder 0 by 3
          line1 += "\\";    //typing the String values in this column
          line2 += " ";
          line3 += "/";
          break;
        case 1:   //when remainder 1 by 3, process repeats for this column
          line1 += " ";
          line2 += "X";
          line3 += " ";
          break;
        case 2:   //when remainder 2 by 3, process repeats for this columnn
          line1 += "/";
          line2 += " ";
          line3 += "\\";
          break;
      }   //ending the switch statement
    }   //ending the for loop
    
      
          System.out.println(line1);    //printing the first, second, and third
          System.out.println(line2);    //lines of code
          System.out.println(line3);
   
    
    
  }
}