///Liv Newman, CSE2, HW 04, due 2/18/19
///This code will randomly generate 5 cards from a deck
///and tell you if you have a pair, two pair, or three of a king
///


//import java.util.Scanner;       //importing Scanner
public class PokerHandCheck{      //importing class
  public static void main(String[] args){    //importing main
    String cardValue1 ="";
    String cardValue2 ="";
    String cardValue3 ="";
    String cardValue4 ="";
    String cardValue5 ="";    //declaring card values
    String Suit1 ="";
    String Suit2 ="";
    String Suit3="";
    String Suit4 ="";
    String Suit5 ="l";          //declaring suits as String
    String hand = "l";
     
      int card1 = (int)((Math.random() * 52)+1); //generating random integer 1-52
      int card2 = (int)((Math.random() * 52)+1); //generating random integer 1-52
      int card3 = (int)((Math.random() * 52)+1); //generating random integer 1-52
      int card4 = (int)((Math.random() * 52)+1); //generating random integer 1-52
      int card5 = (int)((Math.random() * 52)+1); //generating random integer 1-52
    
    
      if(card1 >= 1 && card1 <= 13){    //if card1 is btwn 1-13
        Suit1 = "diamonds";           //then it is a diamond 
      }
      if(card1 >= 14 && card1 <= 26){   //same process for card1
        Suit1 = "clubs";              
      }
      if(card1>= 27 && card1 <= 39){  
        Suit1 = "hearts";             
      }
      if(card1>= 40 && card1 <= 52){  
        Suit1 = "spades";          
      }
   
    switch (card1%13){                //gives the remainder of cardValue1 when divided by 13
       case 0: cardValue1 = "king";    //the value is a King
         break;                       
       case 1: cardValue1 = "ace";      //if the remainder is 1, cardValue1 is an Ace
         break;
       case 2: cardValue1 = "2";      //same process for the remaining card options
         break;
       case 3: cardValue1 = "3";      
         break;
       case 4: cardValue1 = "4";      
         break;
       case 5: cardValue1 = "5";      
         break;
       case 6: cardValue1 = "6";      
         break;
       case 7: cardValue1 = "7";      
         break;
       case 8: cardValue1 = "8";      
         break;
       case 9: cardValue1 = "9";      
         break;
       case 10: cardValue1 = "10";    
         break;
       case 11: cardValue1 = "jack";    
         break;
       case 12: cardValue1 = "queen";      
         break;
     }
    
    
     if(card2>= 1 && card2<= 13){    //if card2 is btwn 1-13
        Suit2 = "diamonds";           //then it is a diamond 
      }
      if(card2>= 14 && card2<= 26){   //same process for card2
        Suit2 = "clubs";              
      }
      if(card2>= 27 && card2<= 39){  
        Suit2 = "hearts";             
      }
      if(card2>= 40 && card2<= 52){  
        Suit2 = "spades";          
      }
    
    switch (card2%13){                //gives the remainder of a cardValue2 when divided by 13
       case 0: cardValue2 = "king";    //the value is a King
         break;                       
       case 1: cardValue2 = "ace";      //if the remainder is 1, card2 is an Ace
         break;
       case 2: cardValue2 = "2";      //same process for the remaining card choices
         break;
       case 3: cardValue2 = "3";      
         break;
       case 4: cardValue2 = "4";      
         break;
       case 5: cardValue2 = "5";      
         break;
       case 6: cardValue2 = "6";      
         break;
       case 7: cardValue2 = "7";      
         break;
       case 8: cardValue2 = "8";      
         break;
       case 9: cardValue2 = "9";      
         break;
       case 10: cardValue2 = "10";    
         break;
       case 11: cardValue2 = "jack";    
         break;
       case 12: cardValue2 = "queen";      
         break;
     }
    
    
    
     if(card3>= 1 && card3<= 13){    //if card3 is btwn 1-13
        Suit3 = "diamonds";           //then it is a diamond 
      }
      if(card3>= 14 && card3<= 26){   //same process for card3
        Suit3 = "clubs";              
      }
      if(card3>= 27 && card3<= 39){  
        Suit3 = "hearts";             
      }
      if(card3>= 40 && card3<= 52){  
        Suit3 = "spades";          
      }
    
      switch (card3%13){                //gives the remainder of a cardValue3 when divided by 13
       case 0: cardValue3 = "king";    //the value is a King
         break;                       
       case 1: cardValue3 = "ace";      //if the remainder is 1, card3 is an Ace
         break;
       case 2: cardValue3 = "2";      //same process for the remaining card choices
         break;
       case 3: cardValue3 = "3";      
         break;
       case 4: cardValue3 = "4";      
         break;
       case 5: cardValue3 = "5";      
         break;
       case 6: cardValue3 = "6";      
         break;
       case 7: cardValue3 = "7";      
         break;
       case 8: cardValue3 = "8";      
         break;
       case 9: cardValue3 = "9";      
         break;
       case 10: cardValue3 = "10";    
         break;
       case 11: cardValue3 = "jack";    
         break;
       case 12: cardValue3 = "queen";      
         break;
     }
    
    
     if(card4>= 1 && card4<= 13){    //if card4 is btwn 1-13
        Suit4 = "diamonds";           //then it is a diamond 
      }
      if(card4>= 14 && card4<= 26){   //same process for card4
        Suit4 = "clubs";              
      }
      if(card4>= 27 && card4<= 39){  
        Suit4 = "hearts";             
      }
      if(card4>= 40 && card4<= 52){  
        Suit4 = "spades";          
      }
    
      switch (card4%13){                //gives the remainder of a cardValue4 when divided by 13
       case 0: cardValue4 = "king";    //the value is a King
         break;                       
       case 1: cardValue4 = "ace";      //if the remainder is 1, card4 is an Ace
         break;
       case 2: cardValue4 = "2";      //same process for the remaining card choices
         break;
       case 3: cardValue4 = "3";      
         break;
       case 4: cardValue4 = "4";      
         break;
       case 5: cardValue4 = "5";      
         break;
       case 6: cardValue4 = "6";      
         break;
       case 7: cardValue4 = "7";      
         break;
       case 8: cardValue4 = "8";      
         break;
       case 9: cardValue4 = "9";      
         break;
       case 10: cardValue4 = "10";    
         break;
       case 11: cardValue4 = "jack";    
         break;
       case 12: cardValue4 = "queen";      
         break;
     }
    
     if(card5>= 1 && card5<= 13){    //if card5 is btwn 1-13
        Suit5 = "diamonds";           //then it is a diamond 
      }
      if(card5>= 14 && card5<= 26){   //same process for card5
        Suit5 = "clubs";              
      }
      if(card5>= 27 && card5<= 39){  
        Suit5 = "hearts";             
      }
      if(card5>= 40 && card5<= 52){  
        Suit5 = "spades";          
      }
    
    
      switch (card5%13){                //gives the remainder of a cardValue5 when divided by 13
       case 0: cardValue5 = "king";    //the value is a King
         break;                       
       case 1: cardValue5 = "ace";      //if the remainder is 1, card5 is an Ace
         break;
       case 2: cardValue5 = "2";      //same process for the remaining card choices
         break;
       case 3: cardValue5 = "3";      
         break;
       case 4: cardValue5 = "4";      
         break;
       case 5: cardValue5 = "5";      
         break;
       case 6: cardValue5 = "6";      
         break;
       case 7: cardValue5 = "7";      
         break;
       case 8: cardValue5 = "8";      
         break;
       case 9: cardValue5 = "9";      
         break;
       case 10: cardValue5 = "10";    
         break;
       case 11: cardValue5 = "jack";    
         break;
       case 12: cardValue5 = "queen";      
         break;
     }
    
    /*      //me including numbers to check my work along the way, but are now irrelevant
    int card1, card2, card3, card4, card5;    
    card1=5;
    card2=4;
    card3=3;
    card4=2;
    card5=4;
    */
    
    if(card1==card2 && card2==card3){       //when the cards are three of a kind
        hand = "three of a kind";           //because of the first card
      }         
    else if(card1==card2 && card2==card4){
      hand = "three of a kind";
    }
    else if(card1==card2 && card3==card5){
      hand = "three of a kind";
    }
    else if(card1==card3 && card3==card4){
      hand = "three of a kind";
    }
    else if(card1==card3 && card3==card5){
      hand = "three of a kind";
    }
    else if(card1==card4 && card4==card5){
      hand = "three of a kind";
    }
   
    else if(card2==card3 && card3==card4){    //when there is a three pair
      hand = "three of a kind";               //because of the second card
    }
    else if(card2==card3 && card3==card5){
      hand = "three of a kind";
    }
    else if(card2==card4 && card4==card5){
      hand = "three of a kind";
    }
   
    else if(card3==card4 && card4==card5){    //when there is a three pair
      hand = "three of a kind";               //because of the third card
    }
    
    //we do not need to include if card5==card4==card3, for example
    //because that is already included if card3==card4==card5
    
    else if(card1==card2 && card3==card4){    //when there are two pair
      hand = "two pair";
    }
    else if(card1==card2 && card3==card5){    
      hand = "two pair";
    }
    else if(card1==card2 && card4==card5){    
      hand = "two pair";
    }
    
    else if(card1==card3 && card4==card5){    
      hand = "two pair";
    }
    else if (card1==card3 && card4==card2){
      hand = "two pair";
    }
    else if(card1==card4 && card2==card5){    
      hand = "two pair";
    }
    
    else if (card1==card4 && card2==card3){
      hand = "two pair";
    }
    else if (card1==card4 && card4==card5){
      hand = "two pair";
    }
    else if (card1==card4 && card3==card5){
      hand = "two pair";
    }
    
    else if (card1==card5 && card2==card3){
      hand = "two pair";
    }
    else if (card1==card5 && card2==card4){
      hand = "two pair";
    }
    else if (card1==card5 && card3==card4){
      hand = "two pair";
    }
    
    else if(card2==card3 && card4==card5){    
      hand = "two pair";
    }
    
    else if(card2==card4 && card3==card5){    
      hand = "two pair";
    }
    
    else if(card1==card2){        //when you have just a single pair
      hand = "a pair";
    }
    else if(card1==card3){    
      hand = "a pair";
    }
    else if(card1==card4){    
      hand = "a pair";
    }
    else if(card1==card5){    
      hand = "a pair";
    }
    else if(card2==card3){    
      hand = "a pair";
    }
    else if(card2==card4){    
      hand = "a pair";
    }
    else if(card2==card5){    
      hand = "a pair";
    }
    else if(card3==card4){    
      hand = "a pair";
    }
    else if(card3==card5){    
      hand = "a pair";
    }
    else if(card4==card5){    
      hand = "a pair";
    }
    
    else if (card1 != card2 && card2 != card3 && card3 != card4 && card4 != card5
            && card1 != card3 && card1 != card4 && card1 != card5
            && card3 != card4 && card3 != card5
            && card4 != card5){                             //when you have a high card hand
      hand = "a high card hand";                            //because no other options were fulfilled
    }
    
    
    System.out.println("Your random cards were:  \n\t the " + cardValue1 + " of " + Suit1);
    System.out.println("\t the " + cardValue2 + " of " + Suit2); 
    System.out.println("\t the " + cardValue3 + " of " + Suit3);
    System.out.println("\t the " + cardValue4 + " of " + Suit4);
    System.out.println("\t the " + cardValue5 + " of " + Suit5);
    System.out.println("You have " + hand + "!");
    
    
  }
  
}