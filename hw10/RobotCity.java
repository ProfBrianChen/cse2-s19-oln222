/// Liv Newman, CSE 2, hw10 
/// this program will ______________
///

public class RobotCity{   //start class
  public static void main(String[] args){   //start main method
    //build city, display it
    int[][] city = buildCity();
    System.out.println("The city is:");
    display(city);
    //invade city, display it
    int k = (int)(Math.random()*100);
    int[][] invadedCity = invade(city, k);
    System.out.println("The invaded city is:");
    display(invadedCity);
    for (int i=0; i<5; i++){  //update and display in a loop 5 times
      System.out.println("The city " + i + " is:");
      update(city);
      display(city);
    }
  }   //end main method
  
  public static int[][] buildCity(){
    //treat top as south and left as east
    int row = (int)(Math.random()*5 + 10);    //IMPORTANT multiplication is the range, and the addition is where it starts
    int column = (int)(Math.random()*5 + 10);
    int[][] city = new int[row][column];
    for(int r=0; r<row; r++){
      for(int c=0; c<column; c++){
        city[r][c] = (int)(Math.random()*800 + 100);
      }
    }
    return city;
  }
  
  
  public static void display(int[][] city){
    //print city's block level populations using printf() format string
    for(int r=0; r<city.length; r++){
      for(int c=0; c<city[r].length; c++){
        System.out.printf("%4d, ", city[r][c]);
      }
      System.out.printf("%n");
    }
    return;
  }
  
  public static int[][] invade(int[][] city, int k){
    //k is # of invading robots
    int row = 0;
    int column = 0;
    for(int i=0; i<k; i++){
      row = (int)(Math.random()*city.length-1);    
      column = (int)(Math.random()*city[0].length-1);
      if(city[row][column] <0){
        row = (int)(Math.random()*city.length-1);    
        column = (int)(Math.random()*city[0].length-1);
      }
      else{
        city[row][column] = -1*city[row][column];
      }
    }
    return city;
  }
  
  public static int[][] update(int[][] city){
    for(int r=0; r<city.length; r++){
      for(int c=0; c<city[r].length; c++){
        if(city[r][c]<0){
          city[r][c] *=-1;    //makes it negative of the same value
          if(c>0){
            city[r][c-1]*=-1;
          }
        }
      }
    }
    return city;
  }
  
}   //end class





