/// Liv Newman, CSE 2, hw10 
/// this program will search for straights
///

public class Straight{   //start class
  public static void main(String[] args){   //start main method
    int n = 0;
    for(int i=0; i<1000000; i++){
      int[] shuffledDeck = generate();
      int[] hand = draw(shuffledDeck);
      boolean endHand = straight(hand);
      if(endHand == true){
        n++;
      }
    }
    System.out.println(n);
    double proportion = ((n/1000000.00)*100.00);
    double trueProp = .3925;
    double difference = trueProp - proportion;
    System.out.println("There is a " + proportion + "% chance of drawing a straight.");
    if(proportion > trueProp){
      System.out.println("This is " + difference + "% more than we expected.");
    }
    else if(proportion < trueProp){
      System.out.println("This is " + difference + "% less than we expected.");
    }
    else{ };
  }   //end main method
  
  
  public static int[] generate(){   //generating a shuffled deck
    int[] original = new int[52];   //declaring og array and shuffled array
    int[] deck = new int[52];
    for (int i=0; i<52; i++){    //assigning values 0-52 to og array
      original[i] = i; 
    }
    for (int i=0; i<52; i++){    //shuffling values into new shuffled array
      int j = (int)((Math.random() * 52)); 
      deck[j] = original[i];
    }
    return deck; 
  } 
  
  
  public static int[] draw(int[] deck){   //selecting first 5 of deck array to make the hand
    int[] hand = {deck[0]%13, deck[1]%13, deck[2]%13, deck[3]%13, deck[4]%13};     //must use % to make cards just #s, regardless of suit
    return hand;
  }
  
  
  public static int search(int[] hand, int k){
    int[] newHand = hand;
    int min1 = 99999;
    int min2 = 99999;
    int min3 = 99999;
    int min4 = 99999;
    int min5 = 99999;
    for(int i=0; i<5; i++){
      if(hand[i] < min1){   //finding highest value
        min5 = min4;
        min4 = min3;
        min3 = min2;
        min2 = min1;
        min1 = hand[i];
      }
      else if(hand[i] < min2){   //finding fourth lowest value
        min5 = min4;
        min4 = min3;
        min3 = min2;
        min2 = hand[i];
      }
      else if(hand[i] < min3){   //finding third lowest value
        min5 = min4;
        min4 = min3;
        min3 = hand[i];
      }
      else if(hand[i] < min4){   //finding second lowest value
        min5 = min4;
        min4 = hand[i];
      }
      else if(hand[i] < min5){   //finding lowest value
        min5 = hand[i];
      }
      else{ }
    }   
      newHand[0] = min1;
      newHand[1] = min2;
      newHand[2] = min3;
      newHand[3] = min4;
      newHand[4] = min5;    //now I have a new, sorted list
    if(k<0 || k>=5){
      System.out.println("Error. Search integer out of bounds.");
      return -1;
    }
    for(int i=0; i<5; i++){
      if(i == k){
        return newHand[i];
      }
    }
    return 0;
  }
  
  
  public static boolean straight(int[] hand){
    for(int i=0; i<4; i++){
      if(search(hand, i) != (search(hand, i+1)-1)){   //if value after current value is not = current value -1, not a straight and it's false
        return false;
      }
    }
    return true;    //if none of that is the case, it's true
  }
  
  
  
}   //end class






