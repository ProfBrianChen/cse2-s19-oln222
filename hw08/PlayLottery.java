/// Liv Newman, CSE 2, hw08 
/// this program will compare the user's numbers to the random lottery numbers generated
///

import java.util.Arrays;    //importing Arrays
import java.util.Scanner;   //importing Scanner
public class PlayLottery{   //opening class
  public static void main(String[] args){   //opening main method
    System.out.print("Enter 5 numbers between 0 and 59: ");   //prompting for user's ticket #static
    Scanner myScanner = new Scanner(System.in);
    int num1 = myScanner.nextInt(); //assigning num# to each input from Scanner
    int num2 = myScanner.nextInt(); 
    int num3 = myScanner.nextInt(); 
    int num4 = myScanner.nextInt(); 
    int num5 = myScanner.nextInt(); 
    //System.out.println(num1 + " " + num2 + " " + num3 + " " + num4 + " " + num5); //checking that it's capturing the right #s
    int[] nums = {num1, num2, num3, num4, num5};    //inputting each entered number into the nums array
    
    int[] numbersPicked = numbersPicked();   //outputting getAtoM method into array AtoM
    System.out.print("The winning numbers are: ");
    for(int i=0; i<5; i++){
      System.out.print(numbersPicked[i] + " ");    //printing getAtoM method
    }
    System.out.println();
    
    if((userWins(nums, numbersPicked))==true){
      System.out.println("You win!!");
    }
    else{
      System.out.println("You lose :/ ");
    }
    
  } //closing main method
  
  
  
  public static boolean userWins(int[] user, int[] winning){  
    //returns true when user and winning are the same
    if(user[0]==winning[0] && user[1]==winning[1] && user[2]==winning[2] && 
       user[3]==winning[3] && user[4]==winning[4]){
      return true;
    }
    else{
      return false;
    }
  }
  
  
  
  public static int[] numbersPicked(){    //
    //generates random #s for lottery w/o replacement
    int lot1 = (int)(Math.random()*60);    //producing random integers from 0 to 59
    int lot2 = (int)(Math.random()*60);
    while(lot2==lot1){    //all of these while loops are checking that the new lottery # 
      lot2=(int)(Math.random()*60);   //will not equal the previous numbers pulled
    } 
    int lot3 = (int)(Math.random()*60);   //and if the newest number does equal a previous one,
    while(lot3==lot2 || lot3==lot1){    //a different random number will be generated until that is no longer the case
      lot3=(int)(Math.random()*60);
    }
    int lot4 = (int)(Math.random()*60);
    while(lot4==lot1 || lot4==lot2 || lot4==lot3){
      lot4=(int)(Math.random()*60);
    }
    int lot5 = (int)(Math.random()*60);
    while(lot5==lot1 || lot5==lot2 || lot5==lot3 || lot5==lot4){
      lot5=(int)(Math.random()*60);
    }
    //System.out.println(lot1 + " " + lot2 + " " + lot3 + " " + lot4 + " " + lot5); //checking all lottery #s are printed correctly
    int[] lotNums = {lot1, lot2, lot3, lot4, lot5};   //inputting the lottery #s into an array
    return lotNums;   //returning to main method
  }   //ending method for determining numbers picked
  
} //closing class







