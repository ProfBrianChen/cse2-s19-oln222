/// Liv Newman, CSE 2, hw08 
/// this program will create an array of uppercase and lowecase letters
/// and reorganize it from a-m and n-z

import java.util.Arrays;    //importing Arrays
public class Letters{   //opening class
  public static void main(String[] args){   //opening main method
    int size = (int)(Math.random()*100);    //producing random size 0 to 99
    //System.out.println(size);   //printing size of array, for my own knowledge
    char[] arrayResult = new char[size];    //declaring array of chars of random size
    for(int i=0; i<size; i++){    //for loop to get each new character
      int var = (int)(Math.random()*57);    //generating random # 0 to 57
      if(var<25){   //for the first 26 numbers,
        arrayResult[i] = (char)('A' + var);  //they will be uppercase letters
      }
      else if(var>=25 && var<=31){    //for the 6 numbers btwn the end of uppercase and start of lowercase,
        i--;    //we need to rerun the loop bc otherwise they print out symbols
      }
      else{   //the last 26 numbers
        arrayResult[i] = (char)('A' + var);   //will be lowercase letters
      }
    }   //ending for loop for each character
    
    System.out.print("Random character array: ");
    for(int i=0; i<size; i++){
        System.out.print(arrayResult[i]);   //printing the original array
    }
    System.out.println();

    System.out.print("AtoM characters: ");
    char[] AtoM = getAtoM(arrayResult);   //outputting getAtoM method into array AtoM
    for(int i=0; i<AtoM.length; i++){
        System.out.print(AtoM[i]);    //printing getAtoM method
    }
    System.out.println();
    
    System.out.print("NtoZ characters: ");
    char[] NtoZ = getNtoZ(arrayResult);   //outputting getAtoM method into array AtoM
    for(int i=0; i<NtoZ.length; i++){
        System.out.print(NtoZ[i]);    //printing getAtoM method
    }
    System.out.println();
  }   //closing main method
  
  public static char[] getAtoM(char[] letter){    //opening getAtoM method
    int size = 0;
    for(int i=0; i<letter.length; i++){   //for loop determining size of array
      if(letter[i]>='a' && letter[i]<='m' ||    
         letter[i]>='A' && letter[i]<='M'){
         size++;
      }
    }   //ending for loop to determine size
    //System.out.println(size);   //printing size of how many chars I should expect in the new array, for my knowledge
    char[] AtoM = new char[size];   //declaring new array AtoM to hold the chars a to m
    int j = 0;
    for(int i=0; i<letter.length; i++){   //for loop comparing each character in the og array
      if(letter[i]>='a' && letter[i]<='m' ||    //to see if it's between a and m, uppercase or lowercase
         letter[i]>='A' && letter[i]<='M'){        
      AtoM[j] = letter[i];
      j++;
      }
    }   //ending for loop to compare each character in the og array
    return AtoM;    //returning to main method
  }   //closing getAtoM method
  
  public static char[] getNtoZ(char[] letter){    //opening getAtoM method
    int size = 0;
    for(int i=0; i<letter.length; i++){   //for loop determining size of array
      if(letter[i]>='n' && letter[i]<='z' ||    
         letter[i]>='N' && letter[i]<='Z'){
         size++;
      }
    }   //ending for loop to determine size
    //System.out.println(size);   //printing size of how many chars I should expect in the new array, for my knowledge
    char[] NtoZ = new char[size];   //declaring new array AtoM to hold the chars a to m
    int j = 0;
    for(int i=0; i<letter.length; i++){   //for loop comparing each character in the og array
      if(letter[i]>='n' && letter[i]<='z' ||    //to see if it's between a and m, uppercase or lowercase
         letter[i]>='N' && letter[i]<='Z'){        
      NtoZ[j] = letter[i];
      j++;
      }
    }   //ending for loop to compare each character in the og array
    return NtoZ;    //returning to main method
  }   //closing getAtoM method
  
}   //closing class



 