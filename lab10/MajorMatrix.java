/// Liv Newman, CSE 2, lab10 
/// this program will print out matrices
///

import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;
public class MajorMatrix{   //declaring class
  public static void main(String args[]){
    int width = (int)(Math.random()*10);
    int length = (int)(Math.random()*10);
    System.out.println("Type 'true' for row-major, type 'false' for column-major");
    Scanner myScanner = new Scanner(System.in); 
    boolean k=myScanner.nextBoolean();
       int [][] matrixa = increasingMatrix(width, length, k);
       int [][] matrixb = increasingMatrix(width, length, k);
       int [][] matrixc = increasingMatrix(width, length, k);
       System.out.println("Matrix A:");
       System.out.println("");
       printMatrix(matrixa);
       System.out.println("Matrix B:");
       System.out.println("");
       printMatrix(matrixb);
       System.out.println("Matrix C:");
       System.out.println("");
       printMatrix(matrixc);
       System.out.println("Sum of matrices:");
       System.out.println("");
       printMatrix( addMatrix( addMatrix(matrixa, matrixb) , matrixc));
    }
  
  
  
  public static void printMatrix(int[][] x){
    for(int i = 0; i < x.length; i++){
      for(int j = 0; j < x[0].length; j++){
        System.out.printf(" %s ", x[i][j]);
        }
      System.out.println(" ");
    }
  }
  
    public static int[][] addMatrix(int[][]x ,int[][] y){
        int[][] sum = new int[x.length][x[0].length];
        if(x.length==y.length && x[0].length==y[0].length){
            for(int i=0;i<x.length;i++){
                for(int j=0;j<x[0].length;j++){
                    sum[i][j]=x[i][j]+y[i][j];
                }

            }
            return sum;
        }
        else{
            return null;
        }  
    }
  
  public static int[][] increasingMatrix(int width, int height, boolean format){
        int[][] matrix= new int[height][width];
        int counter=0;
        if(format==false){
            for(int i=0;i<height;i++){
                for(int j=0;j<width;j++){
                    matrix[i][j]=counter;
                    counter++;
                }
            }
        }
        else{
            for(int i=0;i<width;i++){
                for(int j=0;j<height;j++){
                    matrix[j][i]=counter;
                    counter++;
                }
            }
        }
        return matrix;
    }
  
  
}   //end of class