/// Liv Newman, CSE 2, lab11
/// this program will sort arrays
///

import java.util.Arrays;
public class Sorting{
  public static void main(String[] args){
    int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int iterBest = insertionSort(myArrayBest);
    System.out.println("The total number of operations performed on the reverse sorted array: "
                      + iterBest);
//     int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
//     int iterWorst = insertionSort(myArrayWorst);
//     System.out.println("The total number of operations performed on the sorted array: "
//                       + iterWorst);
  }   //end main method
  
  public static int insertionSort(int[] list) {
    System.out.println(Arrays.toString(list));
    int iterations = 0;
    for (int i=1; i<list.length; i++) {
      iterations++;
      for(int j=i ; j>0 ; j--){
        if(list[j] < list[j-1]){
          int temp = list[j];
          list[j] = list[j-1];
          list[j-1] = temp;
          iterations++;
        }
        else{
          break; 
        }       
      }
      System.out.println(Arrays.toString(list));
    }
    return iterations;
  }
}
  
  
  
  
  
  








