/// Liv Newman, CSE 2, hw06
/// this program will print boxes of different lengths and widths
///

import java.util.Scanner;   //importing Scanner
public class Network{   //opening class
  public static void main(String[] args){   //opening main method
    System.out.println("Input your desired height.");  //asking for height
    Scanner myScanner = new Scanner(System.in);     
    while(!myScanner.hasNextInt()){    //checking that the height is an integer
      System.out.println("Error: please type in an integer.");   //printing error message
      myScanner.next();   //waiting for the value to be input again after the message
    }   //ending while loop
    int height = myScanner.nextInt();
    while(height < 1){    //checking height is positive
      System.out.println("Error: please type in a POSITIVE integer.");
      height = myScanner.nextInt();
    }   //ending while loop
    //now the height is a positive integer
    
    System.out.println("Input your desired width.");  //asking for width
    Scanner myScannerW = new Scanner(System.in);     
    while(!myScannerW.hasNextInt()){    //checking width is an integer
      System.out.println("Error: please type in an integer.");   
      myScannerW.next();   
    }   //ending while loop
    int width = myScannerW.nextInt();
    while(width < 1){    //checking width is positive
      System.out.println("Error: please type in a POSITIVE integer.");
      width = myScanner.nextInt();
    }   //ending while loop
    //now the width is a positive integer
    
    System.out.println("Input square size.");   //asking for square size
    Scanner myScannerS = new Scanner(System.in);     
    while(!myScannerS.hasNextInt()){    //checking square size is an integer
      System.out.println("Error: please type in an integer.");   
      myScannerS.next();   
    }   //ending while loop
    int sqSize = myScannerS.nextInt();
    while(sqSize < 1){    //checking square size is positive
      System.out.println("Error: please type in a POSITIVE integer.");
      sqSize = myScanner.nextInt();
    }   //ending while loop
    //now the square size is a positive integer
    
    System.out.println("Input edge length.");  //asking for edge length
    Scanner myScannerE = new Scanner(System.in);     
    while(!myScannerE.hasNextInt()){    //checking edge length is an integer
      System.out.println("Error: please type in an integer.");   
      myScannerE.next();   
    }   //ending while loop
    int edge = myScannerE.nextInt();
    while(edge < 1){    //checking edge length is positive
      System.out.println("Error: please type in a POSITIVE integer.");
      edge = myScanner.nextInt();
    }   //ending while loop
    //now the edge length is a positive integer
    
    for (int j=1; j<=height; j++){     //determining # of rows to print out
      for (int i=1; i<=width; i++){   //determining # of columns to print out
        if ( (i%(sqSize+edge)==sqSize && j%(sqSize+edge) == 1) //top right corner hashtag
            || i%(sqSize+edge)==1 && j%(sqSize+edge) ==1  //top left corner hashtag
            || i%(sqSize+edge)==sqSize && j%(sqSize+edge)==sqSize   //bottom right corner hashtag
            || i%(sqSize+edge)==1 && j%(sqSize+edge)==sqSize   //bottom left corner hashtag
           ) 
        {  
          System.out.print("#");    //printing corners as hashtags
        }
        else if (i%(sqSize+edge)<sqSize && i%(sqSize+edge)>1 && j%(sqSize+edge)==1  //top row dashes
                || i%(sqSize+edge)<sqSize && i%(sqSize+edge)>1 && j%(sqSize+edge)==sqSize   //bottom row dashes
                || j%(sqSize+edge)==sqSize/2+1 && i%(sqSize+edge)>sqSize  //odd sq size dashes
                || j%(sqSize+edge)==sqSize/2 && i%(sqSize+edge)>sqSize && sqSize%2==0   //even sq size dashes
                || j%(sqSize+edge)==sqSize/2+1 && i%(sqSize+edge)==0    //dashes before new odd pattern
                || j%(sqSize+edge)==sqSize/2 && i%(sqSize+edge)==0 && sqSize%2==0   //dashes before new even pattern
                ) 
          {   
          System.out.print("-");    //printing dashes btwn top and bottom lines of squares and as edges
          }
        else if(j%(sqSize+edge)<sqSize && j%(sqSize+edge)>1 && i%(sqSize+edge)==1   //first column dashes
                || j%(sqSize+edge)<sqSize && j%(sqSize+edge)>1 && i%(sqSize+edge)==sqSize   //last column dashes
                || i%(sqSize+edge)==sqSize/2+1 && j%(sqSize+edge)>sqSize   //odd sq size dashes
                || i%(sqSize+edge)==sqSize/2 && j%(sqSize+edge)>sqSize && sqSize%2==0   //even sq size dashes
                || i%(sqSize+edge)==sqSize/2+1 && j%(sqSize+edge)==0    //dashes before new odd pattern
                || i%(sqSize+edge)==sqSize/2 && j%(sqSize+edge)==0 && sqSize%2==0   //dashes before new even pattern
               ){
          System.out.print("|");    //printing dashes btwn sides of squares and as vertical edges
        }
          
        else{
          System.out.print(" ");  //printing spaces in all other areas
        }
        
      }
      
      
      for (int i=1; i<=edge; i++){
        System.out.print(" ");    //printing spaces for edge length
        }
      System.out.println();
    }
    
    
  }   //closing main method
}   //closing class