/////////
//// CSE 02 Welcome Class
///
public class WelcomeClass{
  
  public static void main(String args []){
     ///prints the welcome message below to terminal window
      System.out.println("  ------------");
      System.out.println("  |  WELCOME |");
      System.out.println("  ------------");
      System.out.println("  ^  ^  ^  ^  ^  ^");
      System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
      System.out.println("<-O--L--N--2--2--2->");
      System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
      System.out.println("  v  v  v  v  v  v");

  }
  
}